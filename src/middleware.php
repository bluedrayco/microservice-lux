<?php

use Slim\App;

return function (App $app) {
    // e.g: $app->add(new \Slim\Csrf\Guard);

    $app->add(new Tuupola\Middleware\JwtAuthentication([
        "secret" => getenv("JWT_SECRET"),
        "secure" => false,
        "path" => '/api',
        "error" => function ($response, $arguments) {
            $data = [];
            $data["status"] = "error";
            $data["message"] = $arguments["message"];

            return $response->withJson($data);
        }
            ]));
        };
        