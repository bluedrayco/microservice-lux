<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->group('/api/v1', function () use ($app) {
        $app->get('/user/{id}/tasks', 'App\Controller\Task:getFromUserId');
        $app->post('/user/{id}/task', 'App\Controller\Task:create');

        $app->post('/user', "App\Controller\User:create");
        $app->get('/users', "App\Controller\User:getAll");
        $app->put('/user/{id}', "App\Controller\User:update");
        $app->delete('/user/{id}', "App\Controller\User:delete");

        $app->put('/user/{user_id}/task/{task_id}', 'App\Controller\Task:update');
        $app->delete('/user/{user_id}/task/{task_id}', 'App\Controller\Task:delete');
    });
};

