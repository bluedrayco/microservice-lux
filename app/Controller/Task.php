<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use RedBeanPHP\Facade as R;
use Exception;

/**
 * Description of User
 *
 * @author tecno
 */
class Task extends BaseController {

    public function getFromUserId(Request $request, Response $response, $args) {
        $tasks = R::findAndExport("task", "user_id= ? and active=1", [$args['id']]);
        return $response->withJson($tasks, 200);
    }

    public function create(Request $request, Response $response, $args) {
        R::begin();
        try {
            $task = R::dispense('task');
            $task->import($request->getParsedBody());
            $task->user_id = $args['id'];
            $task->created_at = new \DateTime();
            $task->updated_at = new \DateTime();
            $task->active = true;
            R::store($task);
            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            return $response->withJson([
                        'error' => $ex->getMessage()
                            ], 404);
        }
        return $response->withJson($task, 200);
    }

    public function update(Request $request, Response $response, $args) {
        R::begin();
        try {
            $task = R::findOne('task', 'id=? and user_id = ? and active=1', [$args['task_id'], $args['user_id']]);
            if (!$task) {
                throw new Exception("The task does not exist.");
            }
            $task->import($request->getParsedBody());
            $task->updated_at = new \DateTime();
            R::store($task);
            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            return $response->withJson([
                        'error' => $ex->getMessage()
                            ], 404);
        }
        return $response->withJson($task, 200);
    }

    public function delete(Request $request, Response $response, $args) {
        R::begin();
        try {
            $task = R::findOne('task', 'id=? and user_id = ? and active=1', [$args['task_id'], $args['user_id']]);
            if (!$task) {
                throw new Exception("The task does not exist.");
            }
            $task->updated_at = new \DateTime();
            $task->active=false;
            R::store($task);
            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            return $response->withJson([
                        'error' => $ex->getMessage()
                            ], 404);
        }
        return $response->withJson([], 204);
    }

}
