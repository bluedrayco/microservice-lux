<?php

namespace App\Controller;

class BaseController{

    protected $ci;

    public function __construct($ci) {
        $this->ci = $ci;
    }
}