<?php

namespace App\Common;

use RedBeanPHP\Facade as R;

class Usuario {

    public static function obtenerBeansInteresesCategoriaSexual($usuario_id) {
        return R::findAll('usuario_interes_categoria_sexual', 'usuario=:usuario_id and activo=true', ['usuario_id' => $usuario_id]);
    }

    public static function obtenerInteresesCategoriaSexual($usuario_id, $columnas = array('categoria_sexo_id', 'usuario_id')) {
        return R::getAll('select ' . implode(',', $columnas) . ' from usuario_interes_categoria_sexual where usuario_id=:usuario_id and activo=true', ['usuario_id' => $usuario_id]);
    }

    public static function obtenerUsuarioById($id) {
        return $sesion = R::findOne("usuario", "id=:id and activo=1", [
                    ':id' => $id,
        ]);
    }

}
