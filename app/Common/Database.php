<?php
namespace App\Common;

class Database{
    
    public static function getStringConnection($parametros){
        $usuario  = $parametros['username'];
        $password = $parametros['password'];
        $conexion = '';
        
        switch($parametros['driver']){
            case 'pgsql':
                $conexion = 'pgsql:dbname='.$parametros['database'].';host='.$parametros['host'];
                break;
            
            case 'sqlite':
                $conexion = 'sqlite:'.$parametros['database'];
                $usuario  = null;
                $password = null;
                break; 
            
            case 'sqlite_memory':
                $conexion = 'sqlite::memory';
                $usuario  = null;
                $password = null;
                break;
            
            case 'mysql':
                $conexion = 'mysql:host='.$parametros['host'].';dbname='.$parametros['database'].($parametros['charset']!=''?(';charset='.$parametros['charset']):'').($parametros['port']!=''?';port='.$parametros['port']:'');
                break;
            
            case 'firebird':
                $conexion = 'firebird:dbname='.$parametros['host'].':'.$parametros['database'];
                break;
            
            case 'informix':
                $conexion = 'informix:'.$parametros['host'].'='.$parametros['database'];
                break;  
            
            case 'oracle':
                $conexion = 'OCI:dbname='.$parametros['database'].($parametros['charset']!=''?(';charset='.$parametros['charset']):'');
                break;
            
            case 'odbc':
                $conexion = 'odbc:Driver={SQL Native Client};Server='.$parametros['host'].';Database='.$parametros['database'].'; Uid='.$parametros['username'].';Pwd='.$parametros['password'].';';
                $usuario  = null;
                $password = null;
                break;
            
            case 'dblib':
                $conexion = 'dblib:host='.$parametros['host'].':'.$parametros['port'].';dbname='.$parametros['database'];
                break;
            
            case 'db2':
                $conexion = 'ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE='.$parametros['database'].'; HOSTNAME='.$parametros['host'].($parametros['port']!=''?';PORT='.$parametros['port']:'').';PROTOCOL=TCPIP;';
                break;
            
            default:
                throw new \Exception('Base de Datos no conocida...');
        }
                
        return $conexion;
    } 
}
