<?php

namespace App\Service;

use RedBeanPHP\Facade as R;

class Sesion extends Base{

    private $session;

    public function __construct(&$session, $ci) {
        parent::__construct($ci);
        $this->session = &$session;
    }

    public function logIn($username, $password) {
        $sesion = R::findOne("usuario", "usuario=:usuario and password=:password and activo=1", [
                    ':usuario' => $username,
                    ':password' => $password
        ]);
        if(!$sesion){
            throw new \Exception("No existe una sesion con ese usuario y password.");
        }
        $sesion->last_login = new \DateTime();
        R::store($sesion);
        $this->session['sesion'] = $sesion->export();
        return $this->session['sesion'];
    }

    public function getSession() {
        return array_key_exists('sesion',$this->session)?$this->session['sesion']:null;
    }

    public function refreshSession() {
        $this->session['sesion']['last_login'] = (new \DateTime())->format("Y-m-d H:i:s");
    }

    public function logout() {
        unset($this->session['sesion']);
    }

}
