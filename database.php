<?php

require __DIR__ . '/vendor/autoload.php';
date_default_timezone_set('America/Mexico_City');

use RedBeanPHP\Facade as R;

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$DATABASE=getenv('DATABASE');
$USERNAME=  getenv('USERNAME');
$PASSWORD=getenv('PASSWORD');
$HOST=  getenv('HOST');
$command = "echo 'CREATE DATABASE IF NOT EXISTS {$DATABASE} CHARACTER SET latin1;'|mysql -u {$USERNAME} -p{$PASSWORD} -h {$HOST}";
echo $command . "\n";
exec($command);
R::setup("mysql:host={$HOST};dbname={$DATABASE}", $USERNAME, $PASSWORD, false);
R::debug(true);
R::selectDatabase("default");

R::ext('xdispense', function( $type ) {
    return R::getRedBean()->dispense($type);
});

$user = R::dispense('users');
$user->name = "Roberto Leroy";
$user->last_name="Monroy";
$user->birthday=new \DateTime("08-09-1989");
$user->active = true;
$user->created_at=new \DateTime();
$user->updated_at=new \DateTime();
R::store($user);

$task = R::dispense("task");
$task->title = "my first task";
$task->detail = "bluedrayco@gmail.com";
$task->user = $user;
$task->active=true;
$task->created_at=new \DateTime();
$task->updated_at=new \DateTime();
R::store($task);


//R::exec("delete from task");
//R::exec("delete from users");

//R::nuke();