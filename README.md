#Instructions for up the system:

First we need to rename .env.example as .env
Modify the .env with the parameters for the database

type:

composer install -o --prefer-dist
php database.php
composer start


#JWT for consume the rest  with the header Authorization Bearer JWT:

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.K1sYwZyzfwGBMHFymGVs0yE6Pv7aSs4CGe8TxswOWhg

Or using docker

docker build -t microservice:1.0 . 
docker run --name microservice -p 8080:80 -v $(pwd):/var/www/microservice -d microservice:1.0